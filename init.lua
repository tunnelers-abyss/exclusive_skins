exclusive_skins = {
	["Hume2"] = {name="Hume2", skin="character_0.png", statue="character0"},
	["CalebJ"] = {name="CalebJ", skin="character_-1.png", statue="character_1"},
	["Coram"] = {name="Coram", skin="character_-2.png", statue="character_2"},
	["Josselin"] = {name="Josselin", skin="character_-3.png", statue="character_3"},
	["LadyK"] = {name="Removed", skin="character_-4.png", statue="character_4"},
	["Vikthor"] = {name="Vikthor", skin="character_-5.png", statue="character_5"},
	["TommyBeeBop"] = {name="TommyBeeBop", skin="character_-6.png", statue="character_6"},
	["Sokomine"] = {name="Sokomine", skin="character_-7.png", statue="character_7"},
	["onePlayer"] = {name="onePlayer", skin="character_-8.png", statue="character_8"},
	["sivarajan"] = {name="sivarajan", skin="character_-9.png", statue="character_9"},
	["Diaeresis"] = {name="Diaeresis", skin="character_-10.png", statue="character_10"},
	["Guill4um"] = {name="Guill4um", skin="character_-11.png", statue="character_11"},
	["Ineva"] = {name="Ineva", skin="character_-12.png", statue = "character_12"},
	["Other_Cody"] = {name="Other_Cody", skin="character_-13.png", statue = "character_13"},
	["pseudonyme"] = {name="pseudonyme", skin="character_-14.png", statue = "character_1758"},
	["nakama"] = {name="nakama", skin="character_-15.png", statue = "character_15"},
	["Edna123"] = {name="Edna123", skin="character_-16.png", statue = "character_16"},
	["alior"] = {name="alior", skin="character_-17.png", statue = "character_17"},
	["Andrii"] = {name="Andrii", skin="character_-18.png", statue = "character_18"},
}

--[[minetest.register_on_joinplayer(function(player)
	local es = exclusive_skins[player:get_player_name()]
	if es then
		if player:get_player_name() == "Diaeresis" then return end
		player:set_properties({
			visual = "mesh",
			textures = {es.skin},
			visual_size = {x=1, y=1},
		})
	end
end)]]

for k, v in pairs(exclusive_skins) do
	if v.statue then
		minetest.register_node("exclusive_skins:" .. v.statue, {
			description = v.name .. " Statue",
			tiles = {v.skin},
			groups = {dig_immediate = 2},
			paramtype = "light",
			drawtype = "mesh",
			paramtype2 = "facedir",
			use_texture_alpha = "clip",
			mesh = "exclusive_skins_statue.obj",
			is_ground_content = false,
			collision_box = {
				type = "fixed",
				fixed = {-0.5, -0.5, -0.5, 0.5, 1.5, 0.5},
			},
			selection_box = {
				type = "fixed",
				fixed = {-0.5, -0.5, -0.5, 0.5, 1.5, 0.5},
			},
		})
	end
end

